from django.db import models
from django.conf import settings


class Project(models.Model):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="projects",
        null=True,
    )
    name = models.CharField(max_length=200)
    description = models.TextField()

    def __str__(self):
        return self.name
