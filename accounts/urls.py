from django.urls import path
from accounts.views import Login_user, Logout_user, Signup_user

urlpatterns = [
    path("login/", Login_user, name="login"),
    path("logout/", Logout_user, name="logout"),
    path("signup/", Signup_user, name="signup"),
]
